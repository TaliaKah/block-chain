# CryptoZombie App

## Introduction

This project was created to learn Solidity and explore blockchain concepts using the Vite framework. The smart contract implementation is based on the tutorials provided by [CryptoZombie](https://cryptozombies.io/en/course).

## Features

- **Wallet Integration:** Connect your wallet to access the app's features.
- **Create Your Zombie:** Generate a zombie with a custom name.
- **Zombie Galleries:** View galleries of your zombies or all zombies on the blockchain managed by our smart contract.
- **Level Up:** Increase your zombie's level by spending testnet currency.
- **Customization:** Change your zombie's name or DNA when your level is sufficiently high.

## Getting Started

Visit [CryptoZombie App](https://block-chain-taliakah-3098e9e3d993e0955854c821ab7ddb4935c708e543.gitlab.io) to start interacting with the app.

## Smart Contract

The smart contract is deployed on the Goerli testnet. You can explore the contract on [Etherscan](https://goerli.etherscan.io/address/0x605761481cD9E1E3e7b85997e27DCAf064C01213). Here's an overview of the contract functionalities:

- **Zombie Attributes:** Each zombie in the contract has specific attributes that can be modified when your zombie reaches a certain level.
- **Actions:** Interact with your zombie by feeding, attacking, or withdrawing it for currency.
- **One Zombie Rule:** Users can create only one zombie.
- **Winning Zombies:** Win other zombies by succeeding in attacks or by feeding your zombie.
- **Cooldown Period:** After feeding or attacking, a cooldown period restricts you from repeating these actions for a specific duration.

## Enjoy Your CryptoZombie Adventure!

Have fun exploring the CryptoZombie App and watch your zombie grow and evolve in the blockchain world.

