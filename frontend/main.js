import './style.css';

import {parseEther} from 'ethers';
import {createPublicClient, createWalletClient, custom, getContract, http} from 'viem';
import {goerli} from 'viem/chains';

import {CryptoZombies} from './abi/CryptoZombies.js';

const [account] =
    await window.ethereum.request({method: 'eth_requestAccounts'});
const walletClient = createWalletClient({
  account,
  chain: goerli,
  transport: custom(window.ethereum),
});

const publicClient = createPublicClient({
  chain: goerli,
  transport: http(),
});

const contractAddress = '0x605761481cD9E1E3e7b85997e27DCAf064C01213';

const cryptoZombiesContract = getContract({
  address: contractAddress,
  abi: CryptoZombies,
  publicClient,
  walletClient,
});

const zombieIds = await cryptoZombiesContract.read.getZombiesByOwner([account]);

const buttonSection = document.querySelector('#button');
const buttonGallerySection = document.querySelector('#gallery');
const warningSection = document.querySelector('#warning');
if (Array.isArray(zombieIds) && zombieIds.length === 0) {
  buttonSection.style.display = 'block';
  buttonGallerySection.style.display = 'none';
  warningSection.style.display = 'none';
} else {
  buttonSection.style.display = 'none';
  buttonGallerySection.style.display = 'block';
  warningSection.style.display = 'none';
}

document.querySelector('#create').addEventListener('click', async () => {
  const name = document.querySelector('#name').value;

  const hash = await cryptoZombiesContract.write.createRandomZombie([name]);

  document.querySelector('#status').innerHTML =
      `Creating a new zombie on the blockchain. This may take a while...`;

  const transaction =
      await publicClient.waitForTransactionReceipt({hash: `${hash}`});

  if (transaction.status === 'success') {
    document.querySelector('#status').innerHTML = `A new zombie is born!`;
  } else {
    document.querySelector('#status').innerHTML =
        'No new zombie is born. Maybe your transaction failed or you already have one.';
  }
});

function zombieName(zombie) {
  return zombie[0];
}
function zombieDNA(zombie) {
  return zombie[1];
}
function zombieLevel(zombie) {
  return zombie[2];
}
function zombieReadyTime(zombie) {
  return zombie[3];
}
function zombieWinCount(zombie) {
  return zombie[4];
}
function zombieLossCount(zombie) {
  return zombie[5];
}

function getColor(deg) {
  return `filter: hue-rotate(${deg}deg);`;
}

function headSrc(i) {
  return `/assets/zombieparts/head-${i}@2x.png`;
}

function eyeSrc(i) {
  return `/assets/zombieparts/eyes-${i}@2x.png`;
}

function shirtSrc(i) {
  return `/assets/zombieparts/shirt-${i}@2x.png`;
}

function zombieHead(dna) {
  return (parseInt(dna.toString().substring(0, 2)) % 7 + 1);
}

function zombieEye(dna) {
  return (parseInt(dna.toString().substring(2, 4)) % 11 + 1);
}

function zombieShirt(dna) {
  return (parseInt(dna.toString().substring(4, 6)) % 6 + 1);
}

function zombieSkinColor(dna) {
  return (parseInt(dna.toString().substring(6, 8)) / 100 * 360);
}

function zombieEyeColor(dna) {
  return (parseInt(dna.toString().substring(8, 10)) / 100 * 360);
}

function zombieClothesColor(dna) {
  return (parseInt(dna.toString().substring(10, 12)) / 100 * 360);
}

function isZombieFedByKitty(zombieDna) {
  return zombieDna % 100 === 99;
}

function createZombieVisual(zombie) {
  const dna = zombieDNA(zombie);
  const zombieContainer = document.createElement('div');
  zombieContainer.classList.add('zombie-parts');

  const headColor = getColor(zombieSkinColor(dna));
  const eyeColor = getColor(zombieEyeColor(dna));
  const clothesColor = getColor(zombieClothesColor(dna));

  if (isZombieFedByKitty()) {
    zombieContainer.innerHTML = `
    <img style="${
        headColor}" class="left-forearm" src="/assets/zombieparts/left-forearm-1@2x.png">  
    <img style="${
        headColor}" class="right-forearm" src="/assets/zombieparts/right-forearm-1@2x.png">

    <img style="${
        headColor}" class="right-upper-arm" src="/assets/zombieparts/right-upper-arm-1@2x.png">  

    <img style="${
        clothesColor}" class="torso" src="/assets/zombieparts/torso-1@2x.png">

    <img style="${
        clothesColor}" class="cat-legs" src="/assets/zombieparts/catlegs.png">
    
    <img style="${clothesColor}" class="shirt" src="${
        shirtSrc(zombieShirt(dna))}" alt="Shirt">

    <img style="${
        headColor}" class="left-upper-arm" src="/assets/zombieparts/left-upper-arm-1@2x.png">  

    <img style="${
        headColor}" class="left-forearm" src="/assets/zombieparts/left-forearm-1@2x.png">  
    <img style="${
        headColor}" class="right-forearm" src="/assets/zombieparts/right-forearm-1@2x.png">

    <img style="${
        headColor}" class="left-hand" src="/assets/zombieparts/hand1-1@2x.png">  
    <img style="${
        headColor}" class="right-hand" src="/assets/zombieparts/hand-2-1@2x.png">

    <img style="${headColor}" class="head" src="${
        headSrc(zombieHead(dna))}" alt="Head">
    <img style="${eyeColor}" class="eye" src="${
        eyeSrc(zombieEye(dna))}" alt="Eyes">
    <img class="mouth" src="/assets/zombieparts/mouth-1@2x.png">
  `;
  } else {
    zombieContainer.innerHTML = `
      <img style="${
        clothesColor}" class="left-feet" src="/assets/zombieparts/left-feet-1@2x.png">  
      <img style="${
        clothesColor}" class="right-feet" src="/assets/zombieparts/right-feet-1@2x.png">  

      <img style="${
        clothesColor}" class="left-leg" src="/assets/zombieparts/left-leg-1@2x.png">  
      <img style="${
        clothesColor}" class="right-leg" src="/assets/zombieparts/right-leg-1@2x.png">  

      <img style="${
        clothesColor}" class="left-thigh" src="/assets/zombieparts/left-thigh-1@2x.png">  
      <img style="${
        clothesColor}" class="right-thigh" src="/assets/zombieparts/right-thigh-1@2x.png">

      <img style="${
        headColor}" class="left-forearm" src="/assets/zombieparts/left-forearm-1@2x.png">  
      <img style="${
        headColor}" class="right-forearm" src="/assets/zombieparts/right-forearm-1@2x.png">

      <img style="${
        headColor}" class="right-upper-arm" src="/assets/zombieparts/right-upper-arm-1@2x.png">  

      <img style="${
        clothesColor}" class="torso" src="/assets/zombieparts/torso-1@2x.png">

      <img style="${clothesColor}" class="shirt" src="${
        shirtSrc(zombieShirt(dna))}" alt="Shirt">

      <img style="${
        headColor}" class="left-upper-arm" src="/assets/zombieparts/left-upper-arm-1@2x.png">  

      <img style="${
        headColor}" class="left-forearm" src="/assets/zombieparts/left-forearm-1@2x.png">  
      <img style="${
        headColor}" class="right-forearm" src="/assets/zombieparts/right-forearm-1@2x.png">

      <img style="${
        headColor}" class="left-hand" src="/assets/zombieparts/hand1-1@2x.png">  
      <img style="${
        headColor}" class="right-hand" src="/assets/zombieparts/hand-2-1@2x.png">

      <img style="${headColor}" class="head" src="${
        headSrc(zombieHead(dna))}" alt="Head">
      <img style="${eyeColor}" class="eye" src="${
        eyeSrc(zombieEye(dna))}" alt="Eyes">
      <img class="mouth" src="/assets/zombieparts/mouth-1@2x.png">
    `;
  }
  return zombieContainer;
}

async function displayZombie(zombie) {
  const zombieContainer = document.createElement('div');
  zombieContainer.classList.add('infos');
  zombieContainer.innerHTML = `
    <p class="info zombie-name">${zombieName(zombie)}</p>
    <p class="info">DNA: ${zombieDNA(zombie)}</p>
    <p class="info">Level: ${zombieLevel(zombie)}</p>
    <p class="info">Wins: ${zombieWinCount(zombie)}</p>
    <p class="info">Losses: ${zombieLossCount(zombie)}</p>
    `;
  const readyInfo = document.createElement('p');

  const currentTimestamp = Math.floor(new Date().getTime() / 1000);

  if (zombieReadyTime(zombie) <= currentTimestamp) {
    readyInfo.textContent = 'Ready to attack !';
  } else {
    const timeLeft = zombieReadyTime(zombie) - currentTimestamp;
    readyInfo.textContent = `Not ready to attack. ${zombieName(zombie)} needs ${
        - timeLeft} seconds to rest.`;
  }

  zombieContainer.appendChild(readyInfo);

  return zombieContainer;
}

async function levelUpZombie(zombieId) {
  const LEVEL_UP_COST_AS_STRING = '0.001';
  const hash = await cryptoZombiesContract.write.levelUp([zombieId], {
    value: parseEther(LEVEL_UP_COST_AS_STRING),
  });

  const transaction =
      await publicClient.waitForTransactionReceipt({hash: `${hash}`});

  if (transaction.status === 'success') {
    console.log(`Zombie ${zombieId} leveled up!`);
  } else {
    console.log(`Zombie did not leveled up!`);
  }
}

function createLevelUpButton(zombieId) {
  const levelUpButton = document.createElement('button');
  levelUpButton.classList.add('changeButton');
  levelUpButton.textContent = 'Level Up';
  levelUpButton.addEventListener('click', async () => {
    if (zombieId !== undefined) {
      await levelUpZombie(zombieId);
      await fetchAndDisplayZombiesOwned();
    }
  });

  return levelUpButton;
}
async function changeZombieName(zombieId, newName) {
  const hash =
      await cryptoZombiesContract.write.changeName([zombieId, newName]);

  const transaction =
      await publicClient.waitForTransactionReceipt({hash: `${hash}`});

  if (transaction.status === 'success') {
    console.log(`Zombie ${zombieId} has changed its name!`);
  } else {
    console.log(`Zombie did not change its name!`);
  }
}

function createNameForm(zombie, zombieId, changeNameButton) {
  const formContainer = document.createElement('div');
  formContainer.classList.add('nameForm');

  const nameLabel = document.createElement('label');
  nameLabel.classList.add('inputText');
  nameLabel.textContent = 'Enter the new name for your zombie:';
  const nameInput = document.createElement('input');
  nameInput.type = 'text';
  nameInput.id = 'newName';
  nameInput.value = zombieName(zombie);

  const submitButton = document.createElement('button');
  submitButton.textContent = 'Submit new Name';

  submitButton.addEventListener('click', async () => {
    const newName = document.querySelector('#newName').value;

    if (newName.trim() !== '') {
      await changeZombieName(zombieId, newName);
      await fetchAndDisplayZombiesOwned();
      changeNameButton.disabled = false;
      formContainer.remove();
    } else {
      alert('Please enter a valid name.');
    }
  });

  formContainer.appendChild(nameLabel);
  formContainer.appendChild(nameInput);
  formContainer.appendChild(submitButton);

  document.querySelector('#zombieGallery').appendChild(formContainer);
}

function createChangeNameButton(zombie) {
  const changeNameButton = document.createElement('button');
  changeNameButton.textContent = 'Change Name';
  changeNameButton.classList.add('changeButton');

  if (zombieLevel(zombie) < 2) {
    changeNameButton.disabled = true;
    changeNameButton.title = 'Zombie must be at least level 2 to change name';
  }

  return changeNameButton;
}

async function changeZombieDNA(zombieId, newDNA) {
  const hash = await cryptoZombiesContract.write.changeDna([zombieId, newDNA]);

  const transaction =
      await publicClient.waitForTransactionReceipt({hash: `${hash}`});

  if (transaction.status === 'success') {
    console.log(`Zombie ${zombieId} have change its dna!`);
  } else {
    console.log(`Zombie did not have change its dna!`);
  }
}

function createChangeDNAButton(zombie, zombieId) {
  const changeDNAButton = document.createElement('button');
  changeDNAButton.textContent = 'Change DNA';
  changeDNAButton.classList.add('changeButton');

  if (zombieLevel(zombie) < 20) {
    changeDNAButton.disabled = true;
    changeDNAButton.title = 'Zombie must be at least level 20 to change DNA';
  }

  return changeDNAButton;
}

function createInput(id, placeholder, value) {
  const input = document.createElement('div');
  input.classList.add('adnPartForm');
  input.innerHTML = `
    <p class="adnInputText">${placeholder}: </p>
    <input id="${id}" class="adnInput" type="number" value = "${value}">`;
  input.value = value;
  return input;
}

async function fetchAndDisplayZombiesOwned() {
  const zombieIds =
      await cryptoZombiesContract.read.getZombiesByOwner([account]);
  const container = document.querySelector('#zombieGallery');
  container.innerHTML = '';  // Vide le contenu actuel

  for (const id of zombieIds) {
    const zombie = await cryptoZombiesContract.read.zombies([id]);
    const zombieVisual = createZombieVisual(zombie);
    container.appendChild(zombieVisual);

    const detailsContainer = document.createElement('div');
    detailsContainer.classList.add('details');
    const infos = await displayZombie(zombie);
    detailsContainer.appendChild(infos);

    const changeDNAButton = createChangeDNAButton(zombie, id);
    changeDNAButton.addEventListener('click', () => {
      changeDNAButton.disabled = true;
      createDNAForm(zombie, id, changeDNAButton);
    });

    const buttonsContainer = document.createElement('div');
    buttonsContainer.classList.add('buttons');
    const levelUpButton = createLevelUpButton(id);
    const changeNameButton = createChangeNameButton(zombie);
    changeNameButton.addEventListener('click', () => {
      changeNameButton.disabled = true;
      createNameForm(zombie, id, changeNameButton);
    });

    buttonsContainer.appendChild(levelUpButton);
    buttonsContainer.appendChild(changeNameButton);
    buttonsContainer.appendChild(changeDNAButton);

    detailsContainer.appendChild(buttonsContainer);

    container.appendChild(detailsContainer);
  }
}

function createDNAForm(zombie, id, changeDNAButton) {
  const formContainer = document.createElement('div');
  formContainer.classList.add('DNAform');

  const dna = zombieDNA(zombie);
  if (id !== undefined && dna !== undefined) {
    const [head, eyes, tshirt, skinColor, eyeColor, clothColor] =
        dna.toString().match(/.{2}/g);
    const dnaPart = document.createElement('div');
    dnaPart.classList.add('DNAparts');

    const createAndAppendInput = (id, label, value) => {
      const input = createInput(id, label, value);
      dnaPart.appendChild(input);
    };

    createAndAppendInput('head', 'Head DNA', head);
    createAndAppendInput('eyes', 'Eyes DNA', eyes);
    createAndAppendInput('tshirt', 'Tshirt DNA', tshirt);
    createAndAppendInput('skinColor', 'Skin color DNA', skinColor);
    createAndAppendInput('eyeColor', 'Eye color DNA', eyeColor);
    createAndAppendInput('clothColor', 'Cloth color DNA', clothColor);

    formContainer.append(dnaPart);
    const submitButton = document.createElement('button');
    submitButton.textContent = 'Submit new DNA';

    submitButton.addEventListener('click', async () => {
      const isValidInput = (input) => input.toString().length == 2;
      if (isValidInput(document.querySelector('#head').value) &&
          isValidInput(document.querySelector('#eyes').value) &&
          isValidInput(document.querySelector('#tshirt').value) &&
          isValidInput(document.querySelector('#skinColor').value) &&
          isValidInput(document.querySelector('#eyeColor').value) &&
          isValidInput(document.querySelector('#clothColor').value)) {
        const newDNA = `${document.querySelector('#head').value}${
            document.querySelector('#eyes').value}${
            document.querySelector('#tshirt').value}${
            document.querySelector('#skinColor').value}${
            document.querySelector('#eyeColor').value}${
            document.querySelector('#clothColor').value}${
            dna.toString().substring(12, 17)}`;
        console.log(BigInt(newDNA));

        await changeZombieDNA(id, BigInt(newDNA));
        await fetchAndDisplayZombiesOwned();
        formContainer.remove();
        changeDNAButton.disabled = false;
      } else {
        alert('Enter a 2 digits number for each DNA part.');
      }
    });

    formContainer.appendChild(submitButton);
  }

  document.querySelector('#zombieGallery').appendChild(formContainer);
}

async function fetchAndDisplayAllZombies() {
  const allZombies = [];
  let counter = 0;
  let zombieExists = true;

  while (zombieExists) {
    try {
      const zombie = await cryptoZombiesContract.read.zombies([counter]);
      allZombies.push(zombie);
      counter++;
    } catch (error) {
      zombieExists = false;
    }
  }

  const container = document.querySelector('#zombieGallery');
  container.innerHTML = '';  // Vide le contenu actuel

  for (const zombie of allZombies) {
    const zombieVisual = createZombieVisual(zombie);
    container.appendChild(zombieVisual);
    const details = await displayZombie(zombie);
    container.appendChild(details);
  };
}

document.querySelector('#displayMyZombiesButton')
    .addEventListener('click', () => {
      fetchAndDisplayZombiesOwned();
      document.querySelector('#displayMyZombiesButton').disabled = true;
      document.querySelector('#displayAllZombiesButton').disabled = false;
    });

document.querySelector('#displayAllZombiesButton')
    .addEventListener('click', () => {
      fetchAndDisplayAllZombies();
      document.querySelector('#displayAllZombiesButton').disabled = true;
      document.querySelector('#displayMyZombiesButton').disabled = false;
    });

fetchAndDisplayAllZombies();