import './style.css';

// 1. Import modules.
import {createPublicClient, createWalletClient, custom, formatUnits, getAddress, getContract, http, parseAbiItem, parseUnits} from 'viem';
import {goerli} from 'viem/chains';

import {UNI} from './abi/UNI';


// 2. Set up your client with desired chain & transport.
const [account] =
    await window.ethereum.request({method: 'eth_requestAccounts'});
const walletClient = createWalletClient({
  account,
  chain: goerli,
  transport: custom(window.ethereum),
});

const publicClient = createPublicClient({
  chain: goerli,
  transport: http(),
});

const token = '0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984';
const tokenLink =
    'https://goerli.etherscan.io/token/0x1f9840a85d5af5bf1d1762f925bdaddc4201f984';

const uniContract = getContract({
  address: token,  // UNI address
  abi: UNI,
  publicClient,
  walletClient,
});

const myAddress = '0xe1fe26d08b15389ecF4BF496f370061ff33d0484';
// await uniContract.write.transfer([myAddress, 1n]);

const name = await uniContract.read.name();
const decimals = await uniContract.read.decimals();
const supply = formatUnits(await uniContract.read.totalSupply(), decimals);
let balance =
    formatUnits(await uniContract.read.balanceOf([myAddress]), decimals);

async function updateBalance() {
  balance = await uniContract.read.balanceOf([account]);
  console.log(`New balance ${balance}`);
  document.querySelector('#balance').innerHTML = `
  <span>Balance of ${myAddress}: ${formatUnits(balance, decimals)}</span>
`;
}

// 3. Consume an action!
publicClient.watchBlockNumber({
  onBlockNumber: async () => {
    const logs = await publicClient.getLogs({
      address: uniContract.address,
      event: parseAbiItem(
          'event Transfer(address indexed from, address indexed to, uint256 value)'),
    });
    const myTransfers = logs.filter(
        (log) => log.args.from == getAddress(account) ||
            log.args.to == getAddress(account));

    if (myTransfers.length > 0) {
      await updateBalance();
    }
  },
});

document.querySelector('#infos').innerHTML = `
  <div>
  <h1>Token UNI</h1>
  <p>Name: ${name}</p>
  <p>Address: <a href = ${tokenLink}>${myAddress}</a></p>
  <p>Total Supply: ${supply}</p>
  </div>
`;

document.querySelector('#balance').innerHTML = `
  <span>Balance of ${myAddress}: ${balance}</span>
`;

document.querySelector('#maxButton').addEventListener('click', () => {
  document.querySelector('#amountInput').value = balance;
});

document.querySelector('#sendButton').addEventListener('click', async () => {
  const amount =
      parseUnits(document.querySelector('#amountInput').value, decimals);
  const recipient = document.querySelector('#recipientInput').value;
  const hashTransaction = await uniContract.write.transfer([recipient, amount]);
  document.querySelector('#status').innerHTML = `
  <div>
    <p>Waiting for tx 
      <a href="https://goerli.etherscan.io/tx/${hashTransaction}">${
      hashTransaction}</a>
    </p>
  </div>
  `
  const transaction =
      await publicClient.waitForTransactionReceipt({hash: hashTransaction});
  if (transaction) {
    document.querySelector('#status').innerHTML = `
    <div>
      <p>Transaction 
        <a href="https://goerli.etherscan.io/tx/${hashTransaction}">${
        hashTransaction}</a>
      </p>
      <p>Confirmed !</p>
    </div>
    `
  } else {
    document.querySelector('#status').innerHTML = `
    <div>
      <p>Transaction 
        <a href="https://goerli.etherscan.io/tx/${hashTransaction}">${
        hashTransaction}</a>
      </p>
      <p>Failed !</p>
    </div>
    `
  }
});