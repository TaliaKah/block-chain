pragma solidity ^0.8.13;

import "lib/forge-std/src/Script.sol";
import "../src/zombiefactory.sol";

contract MyScript is Script {
    function run() external {
        uint256 deployerPrivateKey = vm.envUint("PRIVATE_KEY");
        vm.startBroadcast(deployerPrivateKey);

        ZombieFactory zombieFactory = new ZombieFactory();
        zombieFactory.transferOwnership(0xe1fe26d08b15389ecF4BF496f370061ff33d0484);

        vm.stopBroadcast();
    }
}