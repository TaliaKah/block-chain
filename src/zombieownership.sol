pragma solidity ^0.8.13;

import "./zombieattack.sol";
import "./erc721.sol";

contract ZombieOwnership is ZombieAttack, ERC721{
    function balanceOf(address _owner) external view returns (uint256 _balance){
        return ownerZombieCount[_owner];
    }

    function ownerOf(uint256 _zombieId) external view returns (address _owner){
        return zombieToOwner[_zombieId];
    }

    function _transfer(address _from, address _to, uint256 _zombieId) private{
        ownerZombieCount[_to]++;
        ownerZombieCount[_from]--;
        zombieToOwner[_zombieId] = _to;
        emit Transfer(_from, _to, _zombieId);
    }

    mapping (uint => address) zombieApprovals;

    function transferFrom(address _from, address _to, uint256 _zombieId) external payable {
        require(zombieToOwner[_zombieId] == msg.sender 
            || zombieApprovals[_zombieId] == msg.sender);
        _transfer(_from, _to, _zombieId);
    }

    function approve(address _approved, uint256 _zombieId) external payable onlyOwnerOf(_zombieId){
        zombieApprovals[_zombieId] = _approved;
        emit Approval(msg.sender, _approved, _zombieId);
    }
}